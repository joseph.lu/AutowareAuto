// Copyright 2020 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "gnss_pkg/gnss_pkg_node.hpp"
//lint -e537 NOLINT  // cpplint vs pclint
#include <string>
#include <memory> 
#include "common/types.hpp"



namespace autoware
{
namespace gnss_pkg
{
using std::placeholders::_1; 
using sensor_msgs::msg::NavSatFix;
using geometry_msgs::msg::PoseStamped; 
using autoware::common::types::float64_t;
//using namespace GeographicLib;

GnssPkgNode::GnssPkgNode(const rclcpp::NodeOptions & options)
:  Node("gnss_processor", options),
  verbose(true),
  m_sub_ptr{create_subscription<NavSatFix>(
      "/gnss/fix", rclcpp::QoS{5},
      std::bind(&GnssPkgNode::gnss_handler, this, _1))},
  m_pub_ptr{create_publisher<PoseStamped>("gnss_map_pose", rclcpp::QoS{5})}
{

}

const PoseStamped & GnssPkgNode::process_msg(const PoseStamped & msg) {
  return msg;
}

void GnssPkgNode::gnss_handler (const NavSatFix::SharedPtr msg) {
  std::cerr << "New gnss in~~~~~~~~~~~~~~~~~~~~~~~" << std::endl;

  geo_pos_conv geo;

  geo.set_plane(_plane);
  geo.llh_to_xyz(msg->latitude, msg->longitude, msg->altitude);

  std::cerr << "llh_to_xyz pose: " << geo.x() << ", " << geo.y() << ", " << geo.z() << std::endl;
  PoseStamped pose;
  pose.header = msg->header;
  // pose.header.stamp = ros::Time::now();
  pose.header.frame_id = "map";
  pose.pose.position.x = geo.y();// - 14771;
  pose.pose.position.y = geo.x();// - 84757;
  pose.pose.position.z = geo.z();// + 39;

  std::cerr << "Finish transforminginigngin~" << std::endl;
  // set gnss_stat
  
  if (pose.pose.position.x == 0.0 || pose.pose.position.y == 0.0 || pose.pose.position.z == 0.0)
  {
    gnss_stat_msg = false;
  }
  else
  {
    gnss_stat_msg = true;
  }
  
  double distance = sqrt(pow(pose.pose.position.y - _prev_pose.pose.position.y, 2) +
                         pow(pose.pose.position.x - _prev_pose.pose.position.x, 2));
  std::cout << "distance : " << distance << std::endl;

  //if (distance > 0.2)
  //{
    yaw = atan2(pose.pose.position.y - _prev_pose.pose.position.y, pose.pose.position.x - _prev_pose.pose.position.x);
    //_quat = tf2::createQuaternionMsgFromYaw(yaw);
    tf2::Quaternion _quat;
    _quat.setRPY(0.0, 0.0, yaw);
    _prev_pose = pose;
    _orientation_ready = true;
  //}

  if (_orientation_ready)
  {
    std::cerr << "orienation ready~~~~~~~~~~" << std::endl;
    pose.pose.orientation.x = _quat[0];
    pose.pose.orientation.y = _quat[1];
    pose.pose.orientation.z = _quat[2];
    pose.pose.orientation.w = _quat[3];

    m_pub_ptr->publish(pose);
    //publish_tf(pose);


    
  }

}
/*
void GnssPkgNode::publish_tf(PoseStamped pose){
  tf2_ros::TransformBroadcaster br;

  geometry_msgs::msg::TransformStamped tf_stamped;
  tf_stamped.header.stamp = pose.header.stamp;
  tf_stamped.header.frame_id = "map";
  tf_stamped.child_frame_id = "gnss";
  tf_stamped.transform.translation.x = pose.pose.position.x;
  tf_stamped.transform.translation.y = pose.pose.position.y;
  tf_stamped.transform.translation.z = pose.pose.position.z;

  tf_stamped.transform.rotation.x = pose.pose.orientation.x;
  tf_stamped.transform.rotation.y = pose.pose.orientation.y;
  tf_stamped.transform.rotation.z = pose.pose.orientation.z;
  tf_stamped.transform.rotation.w = pose.pose.orientation.w;
  
  br.sendTransform(tf_stamped);
}
*/
}  // namespace gnss_pkg
}  // namespace autoware
