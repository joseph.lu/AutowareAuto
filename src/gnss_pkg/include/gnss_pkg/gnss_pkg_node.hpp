// Copyright 2020 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// \copyright Copyright 2020 The Autoware Foundation
/// \file
/// \brief This file defines the gnss_pkg_node class.

#ifndef GNSS_PKG__GNSS_PKG_NODE_HPP_
#define GNSS_PKG__GNSS_PKG_NODE_HPP_
#include <gnss_pkg/visibility_control.hpp>

#include <gnss_pkg/gnss_pkg.hpp>
#include <std_msgs/msg/bool.h>
#include <sensor_msgs/msg/nav_sat_fix.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <geometry_msgs/msg/twist.hpp>
#include <geometry_msgs/msg/transform_stamped.hpp>
#include "gnss_pkg/geo_pos_conv.hpp"
#include <tf2_ros/transform_broadcaster.h>

#include <rclcpp/rclcpp.hpp>

#include <string>
#include <tf2/buffer_core.h>
#include <tf2_ros/transform_listener.h>
#include "common/types.hpp"
#include <GeographicLib/Geocentric.hpp>
#include <yaml-cpp/yaml.h>

namespace autoware
{
namespace gnss_pkg
{
using std::placeholders::_1;
using sensor_msgs::msg::NavSatFix;
using geometry_msgs::msg::PoseStamped; 
using geometry_msgs::msg::Quaternion; 

using autoware::common::types::float64_t;


/// \class GnssPkgNode
/// \brief ROS 2 Node for hello world.
class GNSS_PKG_PUBLIC GnssPkgNode : public rclcpp::Node
{
public:
  /// \brief default constructor, starts driver
  /// \param[in] node_name name of the node for rclcpp internals
  /// \throw runtime error if failed to start threads or configure driver
  explicit GnssPkgNode(const rclcpp::NodeOptions & options);

protected: 
  const PoseStamped & process_msg(const PoseStamped & msg);
  void gnss_handler(const NavSatFix::SharedPtr msg);
  //void publish_tf(PoseStamped pose);

private:
  bool verbose;  ///< whether to use verbose output or not.
  typename rclcpp::Subscription<NavSatFix>::SharedPtr m_sub_ptr;
  typename std::shared_ptr<rclcpp::Publisher<PoseStamped>> m_pub_ptr;


  struct geocentric_pose_t
  {
    float64_t latitude;
    float64_t longitude;
    float64_t elevation;
    float64_t roll;
    float64_t pitch;
    float64_t yaw;
  };
  geocentric_pose_t gnss_current;
  PoseStamped m_posestamped_msg;

  int _plane = 7;
  bool _orientation_ready = false;
  bool gnss_stat_msg;

  PoseStamped _prev_pose;
  Quaternion _quat;
  double yaw;
  
};
}  // namespace gnss_pkg
}  // namespace autoware

#endif  // GNSS_PKG__GNSS_PKG_NODE_HPP_
