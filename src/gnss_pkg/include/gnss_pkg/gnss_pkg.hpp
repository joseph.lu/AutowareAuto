// Copyright 2020 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// \copyright Copyright 2020 The Autoware Foundation
/// \file
/// \brief This file defines the gnss_pkg class.

#ifndef GNSS_PKG__GNSS_PKG_HPP_
#define GNSS_PKG__GNSS_PKG_HPP_

#include <gnss_pkg/visibility_control.hpp>
#include <iostream>

namespace autoware
{
/// \brief TODO(joseph.lu): Document namespaces!
namespace gnss_pkg
{

/// \brief TODO(joseph.lu): Document your functions
int32_t GNSS_PKG_PUBLIC print_hello();
/*
void gnss_callback(const sensor_msgs::msg::NavSatFix::SharedPtr msg) {
  RCLCPP_INFO(g_node->get_logger(), "I heard: '%s'", msg->data.c_str());
}
*/
}  // namespace gnss_pkg
}  // namespace autoware

#endif  // GNSS_PKG__GNSS_PKG_HPP_
