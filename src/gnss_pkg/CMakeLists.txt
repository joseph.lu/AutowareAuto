# Copyright 2020 The Autoware Foundation
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
cmake_minimum_required(VERSION 3.5)

project(gnss_pkg)

# dependencies
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)
find_package(GEOGRAPHICLIB REQUIRED)
find_package(ament_cmake_auto REQUIRED)
ament_auto_find_build_dependencies()

set(GEO_POSE_CONV_LIB_SRC
  src/geo_pos_conv.cpp
)

set(GNSS_PKG_LIB_SRC
  src/gnss_pkg.cpp
)


set(GEO_POSE_CONV_LIB_HEADERS
  include/gnss_pkg/geo_pos_conv.hpp
)

set(GNSS_PKG_LIB_HEADERS
  include/gnss_pkg/gnss_pkg.hpp
  include/gnss_pkg/visibility_control.hpp
)

# generate library
ament_auto_add_library(${PROJECT_NAME} SHARED
  ${GEO_POSE_CONV_LIB_SRC}
  ${GEO_POSE_CONV_LIB_HEADERS}
  ${GNSS_PKG_LIB_SRC}
  ${GNSS_PKG_LIB_HEADERS}
)
target_include_directories(${PROJECT_NAME} SYSTEM PUBLIC ${tf2_INCLUDE_DIRS} ${GEOGRAPHICLIB_INCLUDE_DIRS}) #This line helps 
target_link_libraries(${PROJECT_NAME} ${GeographicLib_LIBRARIES})
autoware_set_compile_options(${PROJECT_NAME})

set(GNSS_PKG_NODE_SRC
  src/gnss_pkg_node.cpp
)

set(GNSS_PKG_NODE_HEADERS
  include/gnss_pkg/gnss_pkg_node.hpp
)

# generate component node library
ament_auto_add_library(${PROJECT_NAME}_node SHARED
  ${GNSS_PKG_NODE_SRC}
  ${GNSS_PKG_NODE_HEADERS}
  ${GEO_POSE_CONV_LIB_SRC}
  ${GEO_POSE_CONV_LIB_HEADERS}
)
autoware_set_compile_options(${PROJECT_NAME}_node)
rclcpp_components_register_nodes(${PROJECT_NAME}_node "autoware::gnss_pkg::GnssPkgNode")

set(GNSS_PKG_EXE_SRC
  src/main.cpp
)

# generate stand-alone executable
ament_auto_add_executable(${PROJECT_NAME}_exe
  ${GNSS_PKG_EXE_SRC}
)
autoware_set_compile_options(${PROJECT_NAME}_exe)

# ament package generation and installing
ament_auto_package()
