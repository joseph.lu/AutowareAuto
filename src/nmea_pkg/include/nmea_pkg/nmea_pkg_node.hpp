// Copyright 2020 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/// \copyright Copyright 2020 The Autoware Foundation
/// \file
/// \brief This file defines the nmea_pkg_node class.

#ifndef NMEA_PKG__NMEA_PKG_NODE_HPP_
#define NMEA_PKG__NMEA_PKG_NODE_HPP_

#include <nmea_pkg/nmea_pkg.hpp>

#include <rclcpp/rclcpp.hpp>

#include <string>
//ROS includes
#include <rclcpp/rclcpp.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <nmea_msgs/msg/sentence.hpp>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_ros/static_transform_broadcaster.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

//Autoware includes
#include "nmea_pkg/geo_pos_conv.hpp"

//C++ includes
#include <string>
#include <memory>
#include "math.h"

namespace autoware
{
namespace nmea_pkg
{
using std::placeholders::_1; 
using nmea_msgs::msg::Sentence;
using geometry_msgs::msg::PoseStamped;

/// \class NmeaPkgNode
/// \brief ROS 2 Node for hello world.
class NMEA_PKG_PUBLIC NmeaPkgNode : public rclcpp::Node
{
public:
  /// \brief default constructor, starts driver
  /// \param[in] node_name name of the node for rclcpp internals
  /// \throw runtime error if failed to start threads or configure driver
  explicit NmeaPkgNode(const rclcpp::NodeOptions & options);

  void run();

private:
  bool verbose;  ///< whether to use verbose output or not.

  double toSec (builtin_interfaces::msg::Time time);
  // callbacks
  void callbackFromNmeaSentence(const Sentence::SharedPtr msg);

  // functions
  const PoseStamped & process_msg(const PoseStamped & msg);

  void publishPoseStamped();
  void createOrientation();
  void convert(std::vector<std::string> nmea, builtin_interfaces::msg::Time current_stamp);



  const typename rclcpp::Subscription<Sentence>::SharedPtr m_sub_ptr;
  const typename std::shared_ptr<rclcpp::Publisher<PoseStamped>> m_pub_ptr;


  int32_t plane_number_;
  geo_pos_conv geo_;
  geo_pos_conv last_geo_;
  double roll_, pitch_, yaw_;
  double orientation_time_, position_time_;
  builtin_interfaces::msg::Time current_time_, orientation_stamp_;
  bool orientation_ready_;  // true if position history is long enough to compute orientation

};

std::vector<std::string> split(const std::string &string);

}  // namespace nmea_pkg
}  // namespace autoware

#endif  // NMEA_PKG__NMEA_PKG_NODE_HPP_
