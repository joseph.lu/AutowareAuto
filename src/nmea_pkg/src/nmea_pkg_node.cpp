// Copyright 2020 The Autoware Foundation
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "nmea_pkg/nmea_pkg_node.hpp"

//lint -e537 NOLINT  // cpplint vs pclint
#include <string>

namespace autoware
{
namespace nmea_pkg
{

using std::placeholders::_1; 
using nmea_msgs::msg::Sentence;
using geometry_msgs::msg::PoseStamped;

NmeaPkgNode::NmeaPkgNode(const rclcpp::NodeOptions & options)
:  Node("nmea_processor", options),
  verbose(true),
  m_sub_ptr{create_subscription<Sentence>(
      "/nmea_sentence", rclcpp::QoS{5},
      std::bind(&NmeaPkgNode::callbackFromNmeaSentence, this, _1))},
  m_pub_ptr{create_publisher<PoseStamped>("nmea_map_pose", rclcpp::QoS{5})}
{
}

void NmeaPkgNode::createOrientation()
{
  yaw_ = atan2(geo_.x() - last_geo_.x(), geo_.y() - last_geo_.y());
  roll_ = 0;
  pitch_ = 0;
}

const PoseStamped & NmeaPkgNode::process_msg(const PoseStamped & msg) {
  return msg;
}
void NmeaPkgNode::publishPoseStamped()
{
  PoseStamped pose;
  pose.header.frame_id = "map";
  pose.header.stamp = current_time_;
  pose.pose.position.x = geo_.y();
  pose.pose.position.y = geo_.x();
  pose.pose.position.z = geo_.z();
  tf2::Quaternion q;
  q.setRPY(roll_, pitch_, yaw_);

  pose.pose.orientation.x = q[0];
  pose.pose.orientation.y = q[1];
  pose.pose.orientation.z = q[2]; 
  pose.pose.orientation.w = q[3]; 

  //const auto pose_msg = process_msg(*pose);
  m_pub_ptr->publish(pose);
}

double NmeaPkgNode::toSec(builtin_interfaces::msg::Time time) {
  auto tmp_sec = time.nanosec * 10e-6;
  return time.sec + tmp_sec; 
}


void NmeaPkgNode::callbackFromNmeaSentence(const Sentence::SharedPtr msg) {
  current_time_ = msg->header.stamp;
  convert(split(msg->sentence), msg->header.stamp);

  double timeout = 10.0;
  // if orientation_stamp_ is 0 then no "QQ" sentence was ever received,
  // so orientation should be computed from offsets
  if ((orientation_stamp_.sec == 0 && orientation_stamp_.nanosec == 0)
      || fabs(toSec(orientation_stamp_) - toSec( msg->header.stamp)) > timeout)
  {
    double dt = sqrt(pow(geo_.x() - last_geo_.x(), 2) + pow(geo_.y() - last_geo_.y(), 2));
    double threshold = 0.2;
    if (dt > threshold)
    {
      /* If orientation data is not available it is generated based on translation
         from the previous position. For the first message the previous position is
         simply the origin, which gives a wildly incorrect orientation. Some nodes
         (e.g. ndt_matching) rely on that first message to initialise their pose guess,
         and cannot recover from such incorrect orientation.
         Therefore the first message is not published, ensuring that orientation is
         only calculated from sensible positions.
      */
      if (orientation_ready_)
      {
        std::cerr << "QQ is not subscribed. Orientation is created by atan2" << std::endl;
        createOrientation();
        publishPoseStamped();
      }
      else
      {
        orientation_ready_ = true;
      }
      last_geo_ = geo_;
    }
    return;
  }

  double e = 1e-2;
  if ((fabs(orientation_time_ - position_time_) < e) && orientation_ready_)
  {
    publishPoseStamped();
    return;
  }

}

void NmeaPkgNode::convert(std::vector<std::string> nmea, builtin_interfaces::msg::Time current_stamp)
{
  try
  {
    if (nmea.at(0).compare(0, 2, "QQ") == 0)
    {
      orientation_time_ = stod(nmea.at(3));
      roll_ = stod(nmea.at(4)) * M_PI / 180.;
      pitch_ = -1 * stod(nmea.at(5)) * M_PI / 180.;
      yaw_ = -1 * stod(nmea.at(6)) * M_PI / 180. + M_PI / 2;
      orientation_stamp_ = current_stamp;
      orientation_ready_ = true;
      std::cerr << "QQ is subscribed." << std::endl;
    }
    else if (nmea.at(0) == "$PASHR")
    {
      orientation_time_ = stod(nmea.at(1));
      roll_ = stod(nmea.at(4)) * M_PI / 180.;
      pitch_ = -1 * stod(nmea.at(5)) * M_PI / 180.;
      yaw_ = -1 * stod(nmea.at(2)) * M_PI / 180. + M_PI / 2;
      orientation_ready_ = true;
      std::cerr << "PASHR is subscribed." << std::endl;
    }
    else if (nmea.at(0).compare(3, 3, "GGA") == 0)
    {
      position_time_ = stod(nmea.at(1));
      double lat = stod(nmea.at(2));
      double lon = stod(nmea.at(4));
      double h = stod(nmea.at(9));

      if (nmea.at(3) == "S")
        lat = -lat;

      if (nmea.at(5) == "W")
        lon = -lon;

      geo_.set_llh_nmea_degrees(lat, lon, h);

      std::cerr << "GGA is subscribed." << std::endl;
    }
    else if (nmea.at(0) == "$GPRMC")
    {
      position_time_ = stoi(nmea.at(1));
      double lat = stod(nmea.at(3));
      double lon = stod(nmea.at(5));
      double h = 0.0;

      if (nmea.at(4) == "S")
        lat = -lat;

      if (nmea.at(6) == "W")
        lon = -lon;

      geo_.set_llh_nmea_degrees(lat, lon, h);

      std::cerr << "GPRMC is subscribed." << std::endl;
    }
  }
  catch (const std::exception &e)
  {
    std::cerr << "WARNING: Message is invalid : " << e.what() << std::endl;
  }
}

std::vector<std::string> split(const std::string &string)
{
  std::vector<std::string> str_vec_ptr;
  std::string token;
  std::stringstream ss(string);

  while (getline(ss, token, ','))
    str_vec_ptr.push_back(token);

  return str_vec_ptr;
}

}  // namespace nmea_pkg
}  // namespace autoware
